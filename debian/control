Source: trisycl
Section: devel
Homepage: https://github.com/triSYCL/triSYCL
Priority: optional
Standards-Version: 4.5.0
Vcs-Git: https://salsa.debian.org/opencl-team/trisycl.git
Vcs-Browser: https://salsa.debian.org/opencl-team/trisycl
Maintainer: Debian OpenCL Maintainers <pkg-opencl-devel@lists.alioth.debian.org>
Uploaders: Mo Zhou <lumin@debian.org>
Build-Depends: cmake,
               debhelper-compat (= 12),
               libboost-chrono-dev,
               libboost-dev,
               libboost-log-dev,
               libboost-regex-dev,
               ninja-build,
Rules-Requires-Root: no

Package: libtrisycl-dev
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Suggests: ocl-icd-opencl-dev
Description: Generic modern C++ for heterogeneous platforms with SYCL from Khronos Group
 triSYCL is an open source implementation to experiment with the specification
 of the SYCL C++ layer and to give feedback to the Khronos Group SYCL and
 OpenCL C++ 2.2 kernel language committees and also to the ISO C++ committee.
 .
 This SYCL implementation is mainly based on C++17 and OpenMP or TBB for
 execution on the CPU, with Boost.Compute for the non single-source OpenCL
 interoperability layer and with LLVM/Clang for the device compiler providing
 full single-source SYCL experience, typically targeting a SPIR device. Since in
 SYCL there is a host fall-back, this CPU implementation can be seen as an
 implementation of this fall-back too...
